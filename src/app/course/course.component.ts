import { Component, Input, OnInit } from '@angular/core';
import { Action } from 'rxjs/internal/scheduler/Action';
import { CourseAction } from '../state/course.action';
import { CourseState } from '../state/course.state';


@Component({
  selector: 'course-tag',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
  
})
export class CourseComponent implements OnInit {
  
  constructor(private courseState: CourseState) {}

  @Input() courseName: string = '';
  courses: string[] = [];
  getCoursesClicked() {
    this.courseState.courseDispatcher.next(CourseAction.GET_COURSES);
  }
  clearCoursesClicked() {
    this.courseState.courseDispatcher.next(CourseAction.CLEAR_COURSES);
  }

  ngOnInit(): void {
    this.courseState.courseStore.subscribe(crs => {
      this.courses = crs;
    });
    throw new Error('Method not implemented.');
  }
  
}
