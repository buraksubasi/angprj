export enum CourseAction {
  GET_COURSES,
  CREATE_COURSE,
  GET_COURSE,
  CLEAR_COURSES,
}
