import { htmlAstToRender3Ast } from '@angular/compiler/src/render3/r3_template_transform';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { CourseAction } from './course.action';

@Injectable({
  providedIn: 'root'
})
export class CourseState {
  courseStore: BehaviorSubject<string[]>;
  courseDispatcher: Subject<CourseAction>;

  constructor() {
    this.courseStore = new BehaviorSubject([]);
    this.courseDispatcher = new Subject<CourseAction>();
    this.courseDispatcher.subscribe((action: CourseAction) => {
      if (action === CourseAction.GET_COURSES) {
        this.getCourses();
      } else if (action === CourseAction.CREATE_COURSE) {
        this.register();
      } else if (action === CourseAction.CLEAR_COURSES) {
        this.clearCourses();
      }
    });
  }
  clearCourses() {
    setTimeout(() => {
      this.courseStore.next([]);
    }, 2500);
  }

  register() {
    console.log('register');
  }
  getCourses() {
    setTimeout(() => {
      this.courseStore.next(['course1', 'course2', 'course3', 'course4']);
    }, 2500);
  }
  
}
