import { Component, VERSION } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'Angular ' + VERSION.major;
  arr = ['asd', 'qwe', 'zaxc', 'zbxc', 'azxc', 'bzxc', 'czxc'];
  myvar = 'asd';
  styleMod = { 'background-color': 'green' };
  myMethod() {
    console.log(this.myvar);

  }
}
