import { htmlAstToRender3Ast } from '@angular/compiler/src/render3/r3_template_transform';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthAction } from './auth.action';

@Injectable({
  providedIn: 'root'
})
export class AuthState {
  authStore: BehaviorSubject<string>;
  authDispatcher: Subject<AuthAction>;

  constructor() {
    this.authStore = new BehaviorSubject("");
    this.authDispatcher = new Subject<AuthAction>();
    this.authDispatcher.subscribe((action: AuthAction) => {
      switch (action) {
        case AuthAction.LOGIN:
          this.login();
          break;
        case AuthAction.LOGOUT:
          this.logout();
          break;
      
        default:
          break;
      }
    });
  }

  createToken(lengthOfCode: number, possible: string) {
    let text = '';
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  login() {
    
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,./;'[]=-)(*&^%$#@!~`";
    const lengthOfCode = 40;
    let token = this.createToken(lengthOfCode, possible);
    this.authStore.next(token);
    localStorage.setItem("user token",token)
    console.log("token-->",this.authStore)
  }
  logout() {
    this.authStore.next(null);
    localStorage.removeItem("user token")
    
    console.log("token-->",this.authStore)
  }

  register() {
    console.log('register');
  }
}
