import { Component, Input, OnInit } from '@angular/core';
import { Action } from 'rxjs/internal/scheduler/Action';
import { AuthAction } from '../state/auth.action';
import { AuthState } from '../state/auth.state';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authState: AuthState) {}

  @Input() courseName: string = '';
  token: string = "";
  loginClicked() {
    this.authState.authDispatcher.next(AuthAction.LOGIN);
  }
  logoutClicked() {
    this.authState.authDispatcher.next(AuthAction.LOGOUT)
  }
  ngOnInit(): void {
    this.authState.authStore.subscribe(tkn => {
      this.token = tkn;
    });
    //throw new Error('Method not implemented.');
  }
}
